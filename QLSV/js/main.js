var dssv = [];
const DSSV = "DSSV";
var dataJson = localStorage.getItem(DSSV);
if(dataJson){
    var dataRaw = JSON.parse(dataJson);
    dssv = dataRaw.map(function(item){
        return new SinhVien(
            item.ma,
            item.ten,
            item.email,
            item.matKhau,
            item.diemToan,
            item.diemLy,
            item.diemHoa,
        );
    });
    renderDssv(dssv);
};

// save data
function saveLocalStorage(){
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV, dssvJson);
};

// thêm sinh viên
function themSv(){
    var newSv = layThongTinTuForm();
    var isValid = true;
    // kiểm tra mã sinh viên
    isValid = isValid & kiemTraRong(newSv.ma, "spanMaSV")&& kiemTraMaSv(newSv.ma, dssv, "spanMaSV");
    // kiểm tra tên
    isValid = isValid & kiemTraRong(newSv.ten, "spanTenSV");
    // kiểm tra email
    isValid = isValid & kiemTraRong(newSv.email, "spanEmailSV") && kiemTraEmail(newSv.email,"spanEmailSV");
    // kiểm tra mật khẩu
    isValid = isValid & kiemTraRong(newSv.matKhau, "spanMatKhau");
    // kiểm tra điểm toán, lý, hóa
    isValid = isValid & kiemTraRong(newSv.diemToan, "spanToan");
    isValid = isValid & kiemTraRong(newSv.diemLy, "spanLy");
    isValid = isValid & kiemTraRong(newSv.diemHoa, "spanHoa");

    if(isValid){
        dssv.push(newSv);
        saveLocalStorage();
        renderDssv(dssv);
        resetForm();
    }
};

// xóa sinh Viên
function xoaSv(idSv){
    var index = dssv.findIndex(function(sv){
        return sv.ma == idSv;
    });
    if(index == -1){
        return;
    };
    dssv.splice(index, 1);
    renderDssv(dssv);
    saveLocalStorage();
};

// sửa sinh viên
function suaSv(idSv){
    var index = dssv.findIndex(function(sv){
        return sv.ma === idSv;
    });
    if(index == -1) return;
    var sv = dssv[index];
    showThongTinLenForm(sv);
    document.getElementById("txtMaSV").disable = true;
};

// cập nhật sinh viên
function capNhatSv(){
    var svEdit = layThongTinTuForm();
    var index = dssv.findIndex(function(sv){
        return sv.ma == svEdit.ma;
    });
    if(index == -1) return;
    dssv[index] = svEdit;
    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
    document.getElementById("txtMaSV").disable = false;
};
