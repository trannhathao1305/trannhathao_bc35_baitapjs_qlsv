function layThongTinTuForm(){
    var maSv = document.getElementById("txtMaSV").value.trim();
    var tenSv = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var matKhau = document.getElementById("txtPass").value.trim();
    var diemToan = document.getElementById("txtDiemToan").value.trim();
    var diemLy = document.getElementById("txtDiemLy").value.trim();
    var diemHoa = document.getElementById("txtDiemHoa").value.trim();

    var sv = new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
    return sv;
};

function renderDssv(list) {
    var contentHTML = "";
    for(var i = 0; i < list.length; i++) {
        var currentSv = list[i];
        var contentTr = `
        <tr>
            <td>${currentSv.ma}</td>
            <td>${currentSv.ten}</td>
            <td>${currentSv.email}</td>
            <td>${currentSv.tinhDTB()}</td>
            <td>
                <button onclick="xoaSv('${currentSv.ma}')" class="btn btn-danger">Xóa</button>
                <button onclick="suaSv('${currentSv.ma}')" class="btn btn-primary">Sửa</button>
            </td>
        </tr>`;
        contentHTML = contentHTML + contentTr;
    };
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function showThongTinLenForm(sv){
    document.getElementById("txtMaSV").value = sv.ma;
    document.getElementById("txtTenSV").value = sv.ten;
    document.getElementById("txtEmail").value = sv.email;
    document.getElementById("txtPass").value = sv.matKhau;
    document.getElementById("txtDiemToan").value = sv.diemToan;
    document.getElementById("txtDiemLy").value = sv.diemLy;
    document.getElementById("txtDiemHoa").value = sv.diemHoa;
};

function resetForm() {
    document.getElementById("formQLSV").reset();
  }